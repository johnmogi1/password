# password-validator

## Sela Devops week 2 project submission:

Password validation script using bash.

![my_devops](my_devops.jpg?raw=true?style=centerme "draw by John")

instructions summery:
create a password validation script-
the script will be supplied with an argument - the password.
that argument will be checked for complexity:
small letters, capital, numbers and minimum lenght (10)

Password validation using bash.
[feature] branch - same script with the anbility to retrieve password from a file.
[powershell] branch - same script for powershell

Cheers... john.

“Intelligence is the ability to avoid doing work, yet getting the work done.” ... Linus Torvalds
